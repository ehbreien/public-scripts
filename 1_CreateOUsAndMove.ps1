# User OUs ( logged in as Domain Administrator on DC1 )
New-ADOrganizationalUnit 'AllUsers' -Description 'Contains OUs and users'
New-ADOrganizationalUnit 'IT' -Description 'IT staff' `
-Path 'OU = AllUsers , DC = trel , DC = MaxHus'
New-ADOrganizationalUnit 'PersAdmin' -Description 'Personel admins' `
-Path 'OU = AllUsers , DC = trel , DC = MaxHus'
New-ADOrganizationalUnit 'WHAdmin' -Description 'Warehouse Adminis' `
-Path 'OU = AllUsers , DC = trel , DC = MaxHus'
New-ADOrganizationalUnit 'ShiftLead' -Description 'Shift leaders' `
-Path 'OU = WHAdmin , OU = AllUsers , DC = trel , DC = MaxHus'
New-ADOrganizationalUnit 'WHWorker' -Description 'Warehouse workers' `
-Path 'OU = ShiftLead , OU = WHAdmin , OU = AllUsers , DC = trel , DC = MaxHus'
# Computer OUs
New-ADOrganizationalUnit 'Clients' `
-Description 'Contains OUs and users laptops'
New-ADOrganizationalUnit 'Servers' `
-Description 'Contains OUs and servers'
New-ADOrganizationalUnit 'Admin' -Description 'Adm desktops' `
-Path 'OU = Clients , DC = trel , DC = MaxHus'
New-ADOrganizationalUnit 'Tablets' -Description 'Workers tablets' `
-Path 'OU = Clients , DC = trel , DC = MaxHus'
#Moving computers
Get-ADComputer "MGR" | 
  Move-ADObject -TargetPath "OU=Admin,OU=Clients,DC=trel,DC=MaxHus"
Get-ADComputer "CL1" | 
  Move-ADObject -TargetPath "OU=Tablets,OU=Clients,DC=trel,DC=MaxHus"
Get-ADComputer "SRV1" | 
  Move-ADObject -TargetPath "OU=Servers,DC=trel,DC=MaxHus"

