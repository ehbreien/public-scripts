# on DC1 ( logged in as Domain Administrator )
$GROUP = @{
Name = "g_IT"
GroupCategory = "Security"
GroupScope = "Global"
DisplayName = "Local RDP users"
Path = "OU=IT, OU=AllUsers, DC=trel, DC=MaxHus"
Description = "Users / Groups that will be allowed to RDP to CL1"
}
New-ADGroup @GROUP
$User = Get-ADUser -Filter * -Searchbase 'ou=IT, ou=Allusers, DC=trel, DC=MaxHus' 
Add-ADGroupMember -Identity 'g_IT' -Members $User
#Get-ADGroupMember -Identity 'g_IT'

$GROUP = @{
Name = "g_WHAdmin"
GroupCategory = "Security"
GroupScope = "Global"
DisplayName = "Warehouse managers"
Path = "OU=WHAdmin, OU=AllUsers, DC=trel, DC =MaxHus"
Description = "Users that are hired as Warehouse Manager"
}
New-ADGroup @GROUP
$User = Get-ADUser -Filter * -Searchbase 'ou=WHAdmin, ou=Allusers, DC=trel, DC=MaxHus' 
Add-ADGroupMember -Identity 'g_WHAdmin' -Members $User
#Get-ADGroupMember -Identity 'g_WHAdmin'

$GROUP = @{
Name = "g_PersAdmin"
GroupCategory = "Security"
GroupScope = "Global"
DisplayName = "Personel administrators"
Path = "OU=PersAdmin, OU=AllUsers, DC=trel, DC =MaxHus"
Description = "Users that are hired as POC for Personel"
}
New-ADGroup @GROUP
$User = Get-ADUser -Filter * -Searchbase 'ou=PersAdmin, ou=Allusers, DC=trel, DC=MaxHus' 
Add-ADGroupMember -Identity 'g_PersAdmin' -Members $User
#Get-ADGroupMember -Identity 'g_PersAdmin'

$GROUP = @{
Name = "g_ShiftLead"
GroupCategory = "Security"
GroupScope = "Global"
DisplayName = "Shift Leaders"
Path = "OU=ShiftLead, OU=WHAdmin, OU=AllUsers, DC=trel, DC=MaxHus"
Description = "Users that are shift leaders"
}
New-ADGroup @GROUP
$User = Get-ADUser -Filter * -Searchbase 'ou=ShiftLead, ou=WHAdmin, ou=Allusers, DC=trel, DC=MaxHus' 
Add-ADGroupMember -Identity 'g_ShiftLead' -Members $User
#Get-ADGroupMember -Identity 'g_ShiftLead'

$GROUP = @{
Name = "g_WHWorker"
GroupCategory = "Security"
GroupScope = "Global"
DisplayName = "Warehouse workers."
Path = "OU=WHWorker,OU=ShiftLead, OU=WHAdmin, OU=AllUsers, DC=trel, DC =MaxHus"
Description = "Users that have access to load/pick systems"
}
New-ADGroup @GROUP
$User = Get-ADUser -Filter * -Searchbase 'ou=WHWorker, ou=ShiftLead, ou=WHAdmin, ou=Allusers, DC=trel, DC=MaxHus' 
Add-ADGroupMember -Identity 'g_WHWorker' -Members $User
#Get-ADGroupMember -Identity 'g_WHWorker'

$GROUP = @{
Name = "g_Servers"
GroupCategory = "Security"
GroupScope = "DomainLocal"
DisplayName = "Server database"
Path = "OU=Servers, DC=trel, DC =MaxHus"
Description = "Trel Maxus servers"
}
New-ADGroup @GROUP
$Computer = Get-ADComputer -Identity "SRV1" 
Add-ADGroupMember -Identity 'g_Servers' -Members $Computer
#Get-ADGroupMember -Identity 'g_Servers'

$GROUP = @{
Name = "g_Tablets"
GroupCategory = "Security"
GroupScope = "DomainLocal"
DisplayName = "Workers tablets"
Path = "OU=Tablets, OU=Clients, DC=trel, DC=MaxHus"
Description = "Workers tablets"
}
New-ADGroup @GROUP
$Computer = Get-ADComputer -Identity "Cl1"
Add-ADGroupMember -Identity 'g_Tablets' -Members $Computer
#Get-ADGroupMember -Identity 'g_Tablets'

$GROUP = @{
Name = "g_Admin"
GroupCategory = "Security"
GroupScope = "DomainLocal"
DisplayName = "Admin Computers"
Path = "OU=Admin, OU=Clients, DC=trel, DC=MaxHus"
Description = "Admin computers"
}
New-ADGroup @GROUP
$Computer = Get-ADComputer -Identity "MGR"
Add-ADGroupMember -Identity 'g_Admin' -Members $Computer
#Get-ADGroupMember -Identity 'g_Admin'
