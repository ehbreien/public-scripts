#choco install -y 7zip

curl -O https://download.microsoft.com/download/8/5/C/85C25433-A1B0-4FFA-9429-7E023E7DA8D8Windows%2010%20version%2022H2%20Security%20Baseline.zip

curl -O https://download.microsoft.com/download/8/5/C/85C25433-A1B0-4FFA-9429-7E023E7DA8D8/Windows%2010%20version%2022H2%20Security%20Baseline.zip

7z x .\Windows%2010%20version%2022H2%20Security%20Baseline.zip
cd .\Windows-10-v22H2-Security-Baseline\Scripts
Get-GPO -All | Format-Table -Property displayname
.\Baseline-ADImport.ps1
Get-GPO -All | Format-Table -Property displayname

$OU = "OU=Clients,DC=trel,DC=MaxHus"

# Get all currently linked to OU Clients
# (Repeat this after you have linked to GPOs below)
Get-ADOrganizationalUnit $OU | 
 Select-Object -ExpandProperty LinkedGroupPolicyObjects
# if you want to see the names of the GPOs
# from https://community.spiceworks.com/topic/
#       2197327-powershell-script-to-get-gpo-linked-
#      to-ou-and-its-child-ou
$LinkedGPOs = Get-ADOrganizationalUnit $OU | 
 Select-object -ExpandProperty LinkedGroupPolicyObjects
$LinkedGPOGUIDs = $LinkedGPOs | ForEach-object{$_.Substring(4,36)}
$LinkedGPOGUIDs | 
 ForEach-object {Get-GPO -Guid $_ | Select-object Displayname }

# link two new ones to OU Clients

$OU = 'OU=AllUsers,DC=trel,DC=MaxHus'

Get-GPO -Name "MSFT Windows 10 22H2 - Bitlocker" | 
 New-GPLink -Target $OU
Get-GPO -Name "MSFT Windows 10 22H2 - Computer" | 
 New-GPLink -Target $OU
Get-GPO -Name "MSFT Windows 10 22H2 - Credential Guard" | 
 New-GPLink -Target $OU
Get-GPO -Name "MSFT Windows 10 22H2 - Defender Antivirus" | 
 New-GPLink -Target $OU
Get-GPO -Name "MSFT Windows 10 22H2 - Domain Security" | 
 New-GPLink -Target $OU
Get-GPO -Name "MSFT Windows 10 22H2 - User" | 
 New-GPLink -Target $OU
 
$OU = "OU=Clients,DC=trel,DC=MaxHus"

Get-GPO -Name "MSFT Windows 10 22H2 - Computer" | 
 New-GPLink -Target $OU
Get-GPO -Name "MSFT Windows 10 22H2 - User" | 
 New-GPLink -Target $OU

# On CL1 and/or MGR apply
Invoke-GPUpdate -RandomDelayInMinutes 0
# or 
gpupdate /force

# See changes in report, Policies, Settings, Windows Settings,
# Security Settings, Who is the "Winning GPO"?
gpresult.exe /h GPOReport.html
./GPOReport.html
